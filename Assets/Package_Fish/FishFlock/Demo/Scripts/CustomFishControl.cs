﻿using FishFlock;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomFishControl : MonoBehaviour
{
    [Tooltip("The Flock Controller to modify the fish instances from.")]
    public FishFlockController controller;
    [Tooltip("Minumum acceleration value to keep it non-zero and non-negative.")]
    public float minAccelValue = 5.0f;
    [Tooltip("Minumum speed value to keep it non-zero and non-negative.")]
    public float minSpeedValue = 0.01f;
    [Tooltip("Minumum turn speed value to keep it non-zero and non-negative.")]
    public float minTurnSpeedValue = 2;
    [Tooltip("The value to sum or subtract from the fish's acceleration, speed and turn speed.")]
    public float valueToAffect = 0.5f;
    
    
    public List<Transform> patrolPoints = new List<Transform>();
    BoxCollider colliderPoisson;
    private int i;
    Transform prochainPoint;
    [Tooltip("Mode de patrouille automatique avec points à indiquer dans la liste PatrolPoints")]
    public bool patrolPoint;
    [Tooltip("Mode de changement de point random avec détection du player dans le collider")]
    public bool RandomPoint;
    private int lastPoint;
    public float betweenPointsWaitingTime;
    private FishFlockController scFishFlockController;

    void Start()
    {
        scFishFlockController = GetComponent<FishFlockController>();
    }

    void OnEnable()
    {
        colliderPoisson = GetComponent<BoxCollider>();
        // Register the callback update function on the controller.
        controller.OnUpdateFishEvent += OnUpdateFish;
       
        if(patrolPoint) StartCoroutine(PatrolPoint());
    }

    void OnDisable()
    {
        // Unregister the callback update function from the controller.
        controller.OnUpdateFishEvent -= OnUpdateFish;
    }

    /// <summary>
    /// This function will make the value bounce between positive and negative on an interval of 1.8 seconds.
    /// </summary>
    /// <returns></returns>
    /// 

    private void Update()
    {
        colliderPoisson.center = scFishFlockController.groupAnchor - transform.position;
    }

    IEnumerator InAndOut()
    {
        valueToAffect *= -1.0f;
        yield return new WaitForSeconds(2f);
        scFishFlockController.followTarget = true;
        valueToAffect *= -1.0f;
    }

    /// <summary>
    /// This function receives the current Fish Behaviour that is being looped and modify it's values.
    /// </summary>
    /// <param name="behaviour">FishBehaviour struct containing the fish's data.</param>
    /// <returns>The modified FishBehaviour.</returns>
    public FishBehaviour OnUpdateFish(FishBehaviour behaviour)
    {
        FishBehaviour modifiedBehaviour = behaviour;

        modifiedBehaviour.acceleration -= Random.Range(valueToAffect - (valueToAffect / 2), valueToAffect);
        if (modifiedBehaviour.acceleration <= minAccelValue)
            modifiedBehaviour.acceleration = minAccelValue;
        else if (modifiedBehaviour.acceleration >= controller.maxAcceleration * 2)
            modifiedBehaviour.acceleration = controller.maxAcceleration;

        modifiedBehaviour.speed -= Random.Range(valueToAffect - (valueToAffect / 2), valueToAffect);
        if (modifiedBehaviour.speed <= minSpeedValue)
            modifiedBehaviour.speed = minSpeedValue;
        else if (modifiedBehaviour.speed >= controller.maxSpeed * 2)
            modifiedBehaviour.speed = controller.maxSpeed;

        modifiedBehaviour.turnSpeed -= Random.Range(valueToAffect - (valueToAffect / 2), valueToAffect);
        if (modifiedBehaviour.turnSpeed <= minTurnSpeedValue)
            modifiedBehaviour.turnSpeed = minTurnSpeedValue;
        else if (modifiedBehaviour.turnSpeed >= controller.maxTurnSpeed * 2)
            modifiedBehaviour.turnSpeed = controller.maxTurnSpeed;

        return modifiedBehaviour;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(InAndOut());
            if(RandomPoint) ChangementPoint();
            if(patrolPoint) StartCoroutine(PatrolPoint());
        }
    }
        
    public void ChangementPoint()
    {
        float minX = scFishFlockController.myTransform.position.x - (scFishFlockController.swimmingAreaWidth / 2) + (scFishFlockController.groupAreaWidth / 2);
        float maxX = scFishFlockController.myTransform.position.x + (scFishFlockController.swimmingAreaWidth / 2) - (scFishFlockController.groupAreaWidth / 2);

        float minY = scFishFlockController.myTransform.position.y - (scFishFlockController.swimmingAreaHeight / 2) + (scFishFlockController.groupAreaHeight / 2);
        float maxY = scFishFlockController.myTransform.position.y + (scFishFlockController.swimmingAreaHeight / 2) - (scFishFlockController.groupAreaHeight / 2);

        float minZ = scFishFlockController.myTransform.position.z - (scFishFlockController.swimmingAreaDepth / 2) + (scFishFlockController.groupAreaDepth / 2);
        float maxZ = scFishFlockController.myTransform.position.z + (scFishFlockController.swimmingAreaDepth / 2) - (scFishFlockController.groupAreaDepth / 2);

        float X = Random.Range(minX, maxX);
        float Y = Random.Range(minY, maxY);
        float Z = Random.Range(minZ, maxZ);

        Vector3 nouveauPoint = new Vector3(X, Y, Z);
        scFishFlockController.target.transform.position = nouveauPoint;
    }

    // La fonction doit faire se déplacer les poissons vers le prochain point. Ils y restent quelques secondes (wait for seconds) et vont au prochain point. Au dernier point de la liste, on retourne au premier point.
    IEnumerator PatrolPoint()
    {
        scFishFlockController.followTarget = true;
        lastPoint = patrolPoints.Count;
        Debug.Log(i);
        scFishFlockController.target.transform.position = patrolPoints[i].position;
        if(i == lastPoint) scFishFlockController.target.transform.position = patrolPoints[0].position;
        yield return new WaitForSeconds(betweenPointsWaitingTime);
        i++;
        StartCoroutine(PatrolPoint());
    }

}

