﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flock : MonoBehaviour
{
    public float speed = 0.001f;
    public float rotationSpeed = 5.0f; // How fast the fish will turn
    Vector3 averageHeading; // Average heading direction
    Vector3 averagePosition; // Average position of the group
    float neighbourDistance = 6.0f; // Maximum distance between two fishes (flocking)
    public Vector3 newGoalPos;

    bool turning = false;

    // Start is called before the first frame update
    void Start()
    {
        speed = Random.Range(0.8F, 2.2F); // Donne une vitesse différente pour chaque poisson

        // Ligne à utiliser pour gérer la vitesse de la future animation du poisson
        //this.GetComponent<Animation>()["???"].speed = speed; 
    }

    void OnTriggerEnter(Collider other)
    {
        if(!turning)
        {
            Debug.Log("trigger enter");
            newGoalPos = transform.position - other.gameObject.transform.position;
        }

        turning = true;
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("trigger exit");
        turning = false;
    }


        // Update is called once per frame
    void Update()
    {
        if(turning)
        {
            Vector3 direction = newGoalPos - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation,
                                                  Quaternion.LookRotation(direction),
                                                  rotationSpeed* Time.deltaTime);
            speed = Random.Range(0.8F, 2.2F);

            // Ligne à utiliser pour gérer la vitesse de la future animation du poisson
            //this.GetComponent<Animation>()["???"].speed = speed;
        }
        else
        {
            if (Random.Range(0,10) < 1) // Appelle la fonction ApplyRules 1 fois sur 5
            ApplyRules();
        }

        transform.Translate(0, 0, Time.deltaTime * speed); // déplacement vers l'avant
    }

    void ApplyRules()
    {
        GameObject[] gos;
        gos = globalFlock.allFish; // Gameobjects

        Vector3 vcentre = Vector3.zero;
        Vector3 vavoid = Vector3.zero; // Allow to avoid everything that is near the fish (will be calculated)
        float gSpeed = 0.1f; // Group speed

        Vector3 goalPos = globalFlock.goalPos; 

        float dist;

        int groupSize = 0;
        foreach (GameObject go in gos)
        {
            if(go != gameObject)
            {
                dist = Vector3.Distance(go.transform.position, transform.position);

                if(dist <= neighbourDistance) // Si tu es à moins de "NeighbourDistance", on te compte dans le groupe
                {
                    vcentre += go.transform.position; // On recalcule le centre
                    groupSize++;

                    if(dist < 0.3f) // Si tu es trop près (collide avec un autre poisson)
                    {
                        vavoid = vavoid + (transform.position - go.transform.position); // On te donne une autre direction (éviter la collision) 
                    }

                    flock anotherflock = go.GetComponent<flock>();
                    gSpeed = gSpeed + anotherflock.speed; // Vitesse totale (groupe + poisson)
                }
            }
        }

        if(groupSize > 0)
        {
            vcentre = vcentre / groupSize + (goalPos - transform.position); // Calcul du centre moyen
            speed = gSpeed / groupSize; // Calcul de la vitesse moyenne

            // Ligne à utiliser pour gérer la vitesse de la future animation du poisson
            // this.GetComponent<Animation>()["???"].speed = speed;

            Vector3 direction = (vcentre + vavoid) - transform.position; // Direction du groupe
            if(direction != Vector3.zero)
            {
                // Smooth turn using slerp
                transform.rotation = Quaternion.Slerp(transform.rotation,
                                                      Quaternion.LookRotation(direction),
                                                      rotationSpeed * Time.deltaTime);
            }
        }
    }
}
