﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class globalFlock : MonoBehaviour
{

    public GameObject fishPrefab;
    public GameObject goalPrefab;
    public List<FishGroup> LiFishGroup;

    public Vector3 PosFish = new Vector3();
    public static int tankSize = 5;
    public static int numFish = 50; // Nombre de poissons
    public static GameObject[] allFish = new GameObject[numFish];

    public static Vector3 goalPos = Vector3.zero; // Mise en place d'un point central de la zone (0 est entre -5 et 5)
    // Start is called before the first frame update
    void Start()
    {
        // Pour chaque poisson de l'array, on instantiate un poisson à une position random sur les 3 axes dans une zone
        for (int i = 0; i < numFish; i++)
        {
            Vector3 pos = new Vector3(Random.Range(-tankSize + PosFish.x, tankSize + PosFish.x),
                                      Random.Range(-tankSize + PosFish.y, tankSize + PosFish.y),
                                      Random.Range(-tankSize + PosFish.z, tankSize + PosFish.z));
            allFish[i] = (GameObject)Instantiate(fishPrefab, pos, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Random.Range(0,10000) < 50) // Réinitialise de temps à autre le point central (mais pas trop souvent)
        {
            goalPos = new Vector3(Random.Range(-tankSize + PosFish.x, tankSize + PosFish.x),
                                      Random.Range(-tankSize + PosFish.y, tankSize + PosFish.y),
                                      Random.Range(-tankSize + PosFish.z, tankSize + PosFish.z));
            goalPrefab.transform.position = goalPos;
        }
    }
}

[System.Serializable]

public class FishGroup
{
    public int numFish = 50; // Nombre de poissons
    public Vector3 PosFish = new Vector3();
}
