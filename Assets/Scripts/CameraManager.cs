﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance { get; private set; }

    public List<Target> LiTarget;
    
    [SerializeField] [Range(0f, 4f)] private float distanceSmooth = 0.5f;
    [SerializeField] private AnimationCurve AnimCurveFollow;
    private Vector3 velocity = Vector3.zero;

    private Coroutine shakeCoroutine;

    [HideInInspector] public int IdTarget = 0;

    [SerializeField] private Transform GodRays;

    [Header("Animation Camera")]
    [SerializeField] private List<Transform> LiPosCamGemme;
    [SerializeField] private List<ColorStele> LiScColoStele; 
    private bool isAnimate = false;
    [SerializeField] private AnimationCurve AnimCurveMoveCam;
    [SerializeField] private float DurationMoveCam;
    [SerializeField] private float DurationWaitBetweenAnim;
    [HideInInspector] public bool AnimationIsForGemme = false;

    void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        GodRaysFollow();
    }

    void LateUpdate()
    {
        if(!isAnimate)
            SmoothFollow();
    }

    void SmoothFollow()
    {
        Vector3 toPos = LiTarget[IdTarget].player.position + (LiTarget[IdTarget].player.rotation * LiTarget[IdTarget].defaultDistance);
        //  transform.position = Vector3.SmoothDamp(transform.position, toPos, ref velocity, distanceSmooth);
        transform.position = Vector3.Lerp(transform.position, toPos, Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, LiTarget[IdTarget].player.rotation, Time.deltaTime);
        //transform.LookAt(LiTarget[IdTarget].player, LiTarget[IdTarget].player.up);
    }

    #region Shake
    public void StopAndStartShakeCoroutine(float duration, float magnitude, bool startCoroutine)
    {
        if (shakeCoroutine != null)
            StopCoroutine(shakeCoroutine);
        if(startCoroutine)
            shakeCoroutine = StartCoroutine(ShakeCoroutine(duration, magnitude));
    }

    private IEnumerator ShakeCoroutine(float duration, float magnitude)
    {
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            magnitude += 0.0005f;
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.localPosition = new Vector3(transform.localPosition.x + x, transform.localPosition.y + y, transform.localPosition.z);
            elapsed += Time.deltaTime;

            yield return null;
        }
    }

    public void startAnimCameraToGemme(int idPos)
    {
        StartCoroutine(AnimCameraGemme(idPos));
    }

    private IEnumerator AnimCameraGemme(int idPos)
    {
        isAnimate = true;
        LiTarget[IdTarget].player.GetComponent<Character>().canMove = false;
        LiTarget[IdTarget].player.GetComponent<Character>().CurrentSpeed = 0;
        LiTarget[IdTarget].player.GetComponent<Character>().rb.velocity = Vector3.zero;

        Vector3 startPos = transform.position;
        Quaternion startRot = transform.rotation;
        float startTime = Time.time;

        while(Time.time < startTime + DurationMoveCam)
        {
            transform.position = Vector3.Slerp(startPos, LiPosCamGemme[idPos].position, AnimCurveMoveCam.Evaluate((Time.time - startTime) / DurationMoveCam));
            transform.rotation = Quaternion.Slerp(startRot, LiPosCamGemme[idPos].rotation, AnimCurveMoveCam.Evaluate((Time.time - startTime) / DurationMoveCam));
            yield return null;
        }

        transform.position = LiPosCamGemme[idPos].position;
        transform.rotation = LiPosCamGemme[idPos].rotation;

        if (AnimationIsForGemme)
        {
            yield return new WaitForSeconds(DurationWaitBetweenAnim / 2);
            LiScColoStele[idPos].FxStellaColor.SetBool("ON", true);
            yield return new WaitForSeconds(DurationWaitBetweenAnim * 4);
        }
        else
            yield return new WaitForSeconds(DurationWaitBetweenAnim);


        startTime = Time.time;

        while(Time.time < startTime + DurationMoveCam)
        {
            transform.position = Vector3.Slerp(LiPosCamGemme[idPos].position, startPos, AnimCurveMoveCam.Evaluate((Time.time - startTime) / DurationMoveCam));
            transform.rotation = Quaternion.Slerp(LiPosCamGemme[idPos].rotation, startRot, AnimCurveMoveCam.Evaluate((Time.time - startTime) / DurationMoveCam));
            yield return null;
        }

        transform.position = startPos;
        transform.rotation = startRot;
        LiTarget[IdTarget].player.GetComponent<Character>().canMove = true;
        AnimationIsForGemme = false;
        isAnimate = false;
    }

    private void GodRaysFollow()
    {
        GodRays.position = new Vector3(transform.position.x, GodRays.position.y, transform.position.z);
    }
    #endregion
}

[System.Serializable]
public class Target
{
    public Transform player;
    public Vector3 defaultDistance;
}