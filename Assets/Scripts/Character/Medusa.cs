﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medusa : MonoBehaviour
{
    [Header("Anim Medusa")]
    [SerializeField] private float durationAnimation = 2f;
    [SerializeField] private AnimationCurve animCurve;

    [Header("Light Medusa")]
    [SerializeField] private Light medusaLight;
    [SerializeField] private float durationToMaxLight = 1f;
    [SerializeField] private float MaxIntensity = 10f;
    [HideInInspector] public bool isLightOn = false;


    void Start()
    {
        StartCoroutine(AnimationMedusa());
    }
    
    private IEnumerator AnimationMedusa()
    {
        while (true)
        {
            float startTime = Time.time;
            while(Time.time < startTime + durationAnimation)
            {
                transform.localScale = new Vector3(1f, Mathf.Lerp(1f, 1.2f, animCurve.Evaluate((Time.time - startTime) / durationAnimation)), 1f);
                yield return null;
            }
            transform.localScale = Vector3.one;
           yield return null;
        }
    }

    public IEnumerator MedusaLightOn()
    {
        isLightOn = true;
        float startTime = Time.time;
        float startLight = medusaLight.intensity;
        while (Time.time < startTime + durationToMaxLight)
        {
            medusaLight.intensity = Mathf.Lerp(startLight, MaxIntensity, (Time.time - startTime) / durationToMaxLight);
            yield return null;
        }
        medusaLight.intensity = MaxIntensity;
    }
}
