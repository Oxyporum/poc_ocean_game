﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyAnglerFish : MonoBehaviour
{
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if(other.name == "Baudroie")
            {
                other.GetComponent<AnglerFish>().CaptureBaby(transform);
                GetComponent<SphereCollider>().enabled = false;
            }
        }
    }
}
