﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnglerFishManager : MonoBehaviour
{
    public static AnglerFishManager Instance { get; private set; }
    [SerializeField] private float NumberBaby = 100f;
    private List<GameObject> liBabyAnglerFish;
    public GameObject BabyAnglerFish;


    void Awake()
    {
        Instance = this;
        GenerateBaby();
    }

    void Start()
    {
        
    }
    
    void Update()
    {
        
    }

    private void GenerateBaby()
    {
        liBabyAnglerFish = new List<GameObject>();
        for(int i = 0; i < NumberBaby; i++)
        {
            GameObject baby =  Instantiate(BabyAnglerFish, transform, false) as GameObject;
            baby.transform.position = new Vector3(Random.Range(-10, 10), Random.Range(0, 10), Random.Range(-10, 10)) + transform.position;
            liBabyAnglerFish.Add(baby);
        }
    }
}
