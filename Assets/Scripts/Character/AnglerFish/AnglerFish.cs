﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnglerFish : Character
{

    [SerializeField] private Transform babyPosition;

    private List<Transform> liBabyCapture;

    [Header("Control Baby")]
    [SerializeField] [Range(0f, 4f)] private float distanceSmooth = 0.5f;
    [SerializeField] private float distanceBetweenBaby = 1.2f;
    [SerializeField] private Vector3 defaultDistance = new Vector3(0f, 0f, -1f);
    private Vector3 velocity = Vector3.zero;

    [Header("Ability")]
    [SerializeField] private Light anglerLight;
    [SerializeField] private AnimationCurve AnimCurveFlashUp;
    [SerializeField] private AnimationCurve AnimCurveFlashDown;
    [SerializeField] private float DurationFlashUp;
    [SerializeField] private float DurationFlashDown;
    [SerializeField] private float maxLightIntensity;
    private bool IsFlashing = false;
    private Coroutine flashCoroutine;
    [SerializeField] private List<Transform> LiMedusa;

    public override void Start()
    {
        base.Start();
        liBabyCapture = new List<Transform>();
    }

    public override void Update()
    {
        base.Update();
        if (canMove)
        {
            GetOutCreatureWithInput();
            FlashAbility();
            MoveClampVertical();
        }
        if (liBabyCapture.Count != 0)
        {
         //   foreach
        }
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    private void LateUpdate()
    {
        if (canMove)
        {
            BabyFollow();
        }
    }

    public void CaptureBaby(Transform baby)
    {
        //baby.SetParent(transform, false); // je met le baby en enfant de l'anglerFish
        if (liBabyCapture.Count == 0)
        {
            baby.position = babyPosition.position; // je met le baby à la position de base 
        }
        else
        {
            distanceBetweenBaby += distanceBetweenBaby;
            baby.position = new Vector3(babyPosition.position.x, babyPosition.position.y, babyPosition.position.z + distanceBetweenBaby);
        }
            liBabyCapture.Add(baby);
        CameraManager.Instance.LiTarget[IdPlayer].defaultDistance.z += -2f;
    }

    private void BabyFollow()
    {
        for(int i = 0; i < liBabyCapture.Count; i++)
        {
            Vector3 toPos; 
            if (i == 0)
            {
                toPos = transform.position + (transform.rotation * defaultDistance);
                liBabyCapture[i].LookAt(transform.position, transform.up);
            }
            else
            {
                toPos = liBabyCapture[i - 1].position + (liBabyCapture[i - 1].rotation * defaultDistance);
                liBabyCapture[i].LookAt(liBabyCapture[i - 1].position, liBabyCapture[i - 1].up);
            }
            liBabyCapture[i].position = Vector3.SmoothDamp(liBabyCapture[i].position, toPos, ref velocity, distanceSmooth);
        }
    }

    private void FlashAbility()
    {
        if (buttonAXY)
        {
            if(!IsFlashing && flashCoroutine == null)
            {
                IsFlashing = true;
                flashCoroutine = StartCoroutine(FlashAbilityCoroutine());
            }
        }
    }

    private IEnumerator FlashAbilityCoroutine()
    {
        float startLightIntensity = anglerLight.intensity;
        float startTime = Time.time;
        while(Time.time <= startTime + DurationFlashUp)
        {
            anglerLight.intensity = Mathf.Lerp(startLightIntensity, maxLightIntensity, AnimCurveFlashUp.Evaluate((Time.time - startTime) / DurationFlashUp));
            yield return null;
        }
        anglerLight.intensity = maxLightIntensity;

        LocateMedusa();
        yield return new WaitForSeconds(0.5f);

        startTime = Time.time;
        while(Time.time <= startTime + DurationFlashDown)
        {
            anglerLight.intensity = Mathf.Lerp(maxLightIntensity, startLightIntensity, AnimCurveFlashDown.Evaluate((Time.time - startTime) / DurationFlashDown));
            yield return null;
        }
        anglerLight.intensity = startLightIntensity;
        yield return new WaitForSeconds(0.5f);
        IsFlashing = false;
        flashCoroutine = null;
    }

    private void LocateMedusa()
    {
        foreach(Transform medusa in LiMedusa)
        {
            float distance = Vector3.Distance(medusa.position, transform.position);
            if(distance < 5)
            {
                Medusa scMedusa = medusa.GetComponent<Medusa>();
                if(!scMedusa.isLightOn)
                    StartCoroutine(scMedusa.MedusaLightOn());
            }
        }
    }
}