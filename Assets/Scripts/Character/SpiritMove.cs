﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiritMove : Character
{
    [Header("Switch Character")]
    private Character sc_Character;


    public override void Start()
    {
        base.Start();
        canMove = true;
        IdPlayer = 0;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (canMove)
        {
            MoveNoClamp();
            // DetectionPlayer();
        }
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    private void OnTriggerEnter(Collider other) // switch Character
    {
        if(canMove && other.tag == "Player")
        {
            CurrentSpeed = 0f;
            rb.velocity = Vector3.zero;
            sc_Character = other.GetComponent<Character>();
            sc_Character.canMove = true;
            if (sc_Character.scFishFlockController != null)
                sc_Character.scFishFlockController.enabled = false;
            if (sc_Character.SharkControlate != null)
                sc_Character.SharkControlate.Play();
            CameraManager.Instance.IdTarget = sc_Character.IdPlayer;
            canMove = false;
            transform.position = sc_Character.SpiritPosition.position;
            transform.SetParent(sc_Character.SpiritPosition, true);
            gameObject.SetActive(false);
        }
    }
}
