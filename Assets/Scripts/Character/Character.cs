﻿using System.Collections;
using FishFlock;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [HideInInspector] public Rigidbody rb;

    public int IdPlayer = 0;

    [Header("Movement Player")]
    [SerializeField] private float AccelerationSpeed = 2f;
    [SerializeField] private float DecelerationSpeed = 2f;
    public float MaxSpeed = 10f;
    [HideInInspector] public float CurrentSpeed;
    [HideInInspector] public bool canMove = false;


    private float moveVertical;
    private float moveHorizontal;
    private bool canAccelerate = false;
    private bool buttonEscape = false;
    private bool buttonB = false;
    [HideInInspector] public bool buttonAXY = false;

    [Header("Rotation player")]
    [SerializeField] private float rotationSpeed = 10f;
    [SerializeField] private float maxAngleVertical = 75f;
    [SerializeField] private float minAngleVertical = -75f;
    [SerializeField] private float maxAngleHoritontal = 30f;
    [SerializeField] private float minAngleHorizontal = -30f;
    private bool canMoveUp = true;
    private bool canMoveDown = true;

    [Header("Switch player")]
    public Transform SpiritPosition = null;
    [SerializeField] private float delayInput = 2f;
    private float duration = 0f;
    private bool isInputB = false;
    [HideInInspector] public FishFlockController scFishFlockController;

    [Header("Ability")]
    [HideInInspector] public bool SharkIsBoost = false;

    [Header("Animation Controle")]
    public ParticleSystem SharkControlate;

    private Animator animator;


    #region Unity Methode
    public virtual void Awake()
    {
        
    }

    public virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
       // if(GetComponent<FishFlockController>() != null)
          //  scFishFlockController = GetComponent<FishFlockController>();
    }


    public virtual void Update()
    {
        if (canMove)
        {
            UpdateControl();
            PauseInput();
            AccelerationDeceleration(canAccelerate);
        }
    }

    public virtual void FixedUpdate()
    {
      //  if (canMove)
        //    AccelerationDeceleration(canAccelerate);
    }

    #endregion

    public virtual void UpdateControl()
    {
        moveVertical = Input.GetAxis("Vertical");
        moveHorizontal = Input.GetAxis("Horizontal");
        buttonEscape = Input.GetButtonUp("Cancel");
        if (Input.GetButton("Fire2")) // get out of creature
            buttonB = true;
        else
            buttonB = false;

        if (Input.GetButton("Fire3") || Input.GetButton("Jump") || Input.GetButton("Fire1")) // ability
            buttonAXY = true;
        else
            buttonAXY = false;

        if (Input.GetAxis("RT Button") < -0.5f || Input.GetAxis("LT Button") > 0.5f || Input.GetButton("RB Button") || Input.GetButton("LB Button"))
            canAccelerate = true;
        else
            canAccelerate = false;
    }

    private void PauseInput()
    {
        if (buttonEscape)
        {
            buttonEscape = false;
            if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
                // active UI menu pause
            }
            else
            {
                Time.timeScale = 0;
                // unactive UI menu pause
            }
        }
    }

    #region Movement PLayer
    protected void MoveClampVertical()
    {
        float clampedValueX = Mathf.Clamp((transform.localEulerAngles.x <= 180) ? transform.localEulerAngles.x : -(360 - transform.localEulerAngles.x), minAngleVertical, maxAngleVertical);
        float clampedValueZ = Mathf.Clamp((transform.localEulerAngles.z <= 180) ? transform.localEulerAngles.z : -(360 - transform.localEulerAngles.z), minAngleHorizontal, maxAngleHoritontal);
        Vector3 RotInit = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);

        transform.localEulerAngles = new Vector3(clampedValueX, transform.localEulerAngles.y, clampedValueZ);

        if (moveHorizontal >= 0.1f || moveHorizontal <= -0.1f) // rotation horizontal
        {
            transform.Rotate(0f, (Mathf.Sign(moveHorizontal) * rotationSpeed * Time.deltaTime), 0f, Space.World);
            transform.Rotate(0f, 0f, (-Mathf.Sign(moveHorizontal) * rotationSpeed * Time.deltaTime), Space.Self);

            if (IdPlayer == 1)
            {
                if (moveHorizontal >= 0.1f)
                    animator.SetBool("right", true);
                if (moveHorizontal <= -0.1f)
                    animator.SetBool("left", true);
            }
        }
        else
        {
            if (transform.localEulerAngles.z < -0.1f || transform.localEulerAngles.z > 0.1f)
                transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(RotInit), Time.deltaTime);
            else
                transform.localEulerAngles = RotInit;
        }
        
        if (moveVertical >= 0.1f)
        {
            if(((clampedValueX > minAngleVertical && clampedValueX < maxAngleVertical) || !canMoveDown) && canMoveUp)
            {
                transform.Rotate(Mathf.Sign(moveVertical) * -1 * rotationSpeed * Time.deltaTime, 0f, 0f, Space.Self);
                if (IdPlayer == 1)
                    animator.SetBool("top", true);
                canMoveDown = true;
            }
            else
                canMoveUp = false;
        }

        if( moveVertical <= -0.1f)
        {
            if(((clampedValueX > minAngleVertical && clampedValueX < maxAngleVertical) || !canMoveUp) && canMoveDown)
            {
                transform.Rotate(Mathf.Sign(moveVertical) * -1 * rotationSpeed * Time.deltaTime, 0f, 0f, Space.Self);
                if (IdPlayer == 1)
                    animator.SetBool("bot", true);
                canMoveUp = true;
            }
            else
                canMoveDown = false;
        }

        if(IdPlayer == 1)
        {
            if (moveHorizontal <= 0.1f)
                animator.SetBool("right", false);
            if (moveHorizontal >= -0.1f)
                animator.SetBool("left", false);
            if (moveVertical <= 0.1f)
                animator.SetBool("top", false);
            if (moveVertical >= -0.1f)
                animator.SetBool("bot", false);
        }

        //AccelerationDeceleration(canAccelerate);
    }

    protected void MoveNoClamp()
    {
        if (moveHorizontal >= 0.1f || moveHorizontal <= -0.1f) // rotation horizontal
            transform.Rotate(0f, (Mathf.Sign(moveHorizontal) * rotationSpeed * Time.deltaTime), 0f, Space.World);

        if(moveVertical >= 0.1f || moveVertical <= -0.1f)
            transform.Rotate(Mathf.Sign(moveVertical) * -1 * rotationSpeed * Time.deltaTime, 0f, 0f, Space.Self);

        if (moveHorizontal <= 0.1f && moveHorizontal >= -0.1f && moveVertical <= 0.1f && moveVertical >= -0.1f)
        {
            // transform.Rotate(0, 0f, 0f, Space.World);
        }
        //AccelerationDeceleration(canAccelerate);
    }

    private void AccelerationDeceleration(bool isMoving)
    {
        if (!SharkIsBoost)
        {
            if (isMoving)
            {
                CurrentSpeed += AccelerationSpeed * Time.fixedDeltaTime;
                if (CurrentSpeed >= MaxSpeed)
                    CurrentSpeed = MaxSpeed;
            }
            else
            {
                CurrentSpeed -= DecelerationSpeed * Time.fixedDeltaTime;
                if (CurrentSpeed <= 0f)
                    CurrentSpeed = 0f;
            }
        }
        rb.velocity = transform.forward * CurrentSpeed;

        if(IdPlayer == 1) // change animation shark speed 
        {
            if (CurrentSpeed > MaxSpeed)
            {
                float speedAnim = CurrentSpeed / MaxSpeed + 1;
                animator.SetFloat("speed", speedAnim);
            }
            else if (CurrentSpeed == 0)
                animator.SetFloat("speed", 0.3f);
            else
                animator.SetFloat("speed", 1);
        }
    }
    #endregion

    public virtual void GetOutCreatureWithInput()
    {
        if (buttonB && !SharkIsBoost)
        {
            duration += Time.deltaTime;
            if (!isInputB)
            {
                isInputB = true;
                CameraManager.Instance.StopAndStartShakeCoroutine(delayInput, 0.001f, true);
            }
            if(duration >= delayInput)
            {
                duration = 0;
                GetOutCreature();
            }
        }
        else
        {
            duration = 0;
            isInputB = false;
            CameraManager.Instance.StopAndStartShakeCoroutine(delayInput, 0.005f, false);
        }
    }

    public void GetOutCreature()
    {
        Debug.Log("get out");
        SpiritPosition.GetChild(0).gameObject.SetActive(true);
        SpiritMove spirit = FindObjectOfType<SpiritMove>().GetComponent<SpiritMove>();
        spirit.transform.parent = null;
        spirit.transform.localScale = Vector3.one;
        spirit.transform.rotation = transform.rotation;
        CurrentSpeed = 0f;
        CameraManager.Instance.IdTarget = 0;
        canMove = false;
        scFishFlockController.enabled = true;
        rb.velocity = Vector3.zero;
        spirit.canMove = true;
        CameraManager.Instance.StopAndStartShakeCoroutine(delayInput, 0.005f, false);
    }
}