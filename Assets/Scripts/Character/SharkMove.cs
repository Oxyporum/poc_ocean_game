﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkMove : Character
{
    [Header("Ability")]
    [SerializeField] private float durationAccelerationBoost = 2f;
    [SerializeField] private float durationDecelerationBoost = 0.5f;
    [SerializeField] private float maxBoostSpeed = 60f;
    [SerializeField] private AnimationCurve animCurveBoostAcceleration;
    [SerializeField] private AnimationCurve animCurvBoostDeceleration;
    [SerializeField] private float durationMaxBoostSpeed;
    [SerializeField] private float boostCooldown = 2f;
    private Coroutine boostCoroutine;

    [Header("Particle")]
    [SerializeField] private ParticleSystem SharkBuble;

    public override void Start()
    {
        base.Start();
        canMove = false;
        IdPlayer = 1;
    }

    public override void Update()
    {
        base.Update();
        if (canMove)
        {
            GetOutCreatureWithInput();
            BoostAbility();
            MoveClampVertical();
        }
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    private void BoostAbility()
    {
        if(buttonAXY)
        {
            if(!SharkIsBoost && boostCoroutine == null)
            {
                SharkIsBoost = true;
                boostCoroutine = StartCoroutine(BoostCoroutine());
            }
        }
    }

    private IEnumerator BoostCoroutine()
    {
        SharkBuble.Play();

        float startspeed = CurrentSpeed;
        float startTime = Time.time;
        while(Time.time < startTime + durationAccelerationBoost)
        {
            CurrentSpeed = Mathf.Lerp(startspeed, maxBoostSpeed, animCurveBoostAcceleration.Evaluate((Time.time - startTime) / durationAccelerationBoost));
            yield return null;
        }
        CurrentSpeed = maxBoostSpeed;

        yield return new WaitForSeconds(durationMaxBoostSpeed);

        startTime = Time.time;
        startspeed = CurrentSpeed;
        while(Time.time < startTime + durationDecelerationBoost)
        {
            CurrentSpeed = Mathf.Lerp(startspeed, MaxSpeed, animCurvBoostDeceleration.Evaluate((Time.time - startTime) / durationDecelerationBoost));
            yield return null;
        }
        CurrentSpeed = MaxSpeed;
        SharkIsBoost = false;
        yield return new WaitForSeconds(boostCooldown);
        boostCoroutine = null;
    }

}
