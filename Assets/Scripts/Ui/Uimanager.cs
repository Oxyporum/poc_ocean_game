﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Uimanager : MonoBehaviour
{
    public static Uimanager Instance { get; private set; }

    [SerializeField] private GameObject StartScreen;
    [SerializeField] private GameObject PauseScreen;
    [SerializeField] private GameObject ControlScreen;

    private void Awake()
    {
        Instance = this;
    }

    public void StartGame()
    {
        StartScreen.SetActive(false);
        Cursor.visible = false;
    }

    public void ShowControl()
    {
        StartScreen.SetActive(false);
        ControlScreen.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

 
}