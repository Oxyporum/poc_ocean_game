﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorStone : MonoBehaviour
{
    [SerializeField] private bool blue;
    [SerializeField] private bool red;
    [SerializeField] private bool green;
    [SerializeField] private int idPlayer;

    [Header("Animation Gemme")]
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float durationMove;
    [SerializeField] private float maxGemmePos;
    [SerializeField] private AnimationCurve AnimCurvMoveGemme;
    [SerializeField] private ParticleSystem fxGemmeRecup;

    void Start()
    {
        StartCoroutine(AnimationGemme());
    }

    // Update is called once per frame
    void Update()
    {
        RotationGemme();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && CameraManager.Instance.IdTarget != 0)
        {
            Character sc_Character = other.GetComponent<Character>();
            if (sc_Character.canMove)
            {
                if (blue && sc_Character.IdPlayer == idPlayer)
                {
                    ColorManager.Instance.playerGotBlue = true;
                    CameraManager.Instance.AnimationIsForGemme = true;
                    CameraManager.Instance.startAnimCameraToGemme(0);
                }
                else if (red && sc_Character.IdPlayer == idPlayer)
                {
                    ColorManager.Instance.playerGotRed = true;
                    CameraManager.Instance.AnimationIsForGemme = true;
                    CameraManager.Instance.startAnimCameraToGemme(1);
                }
                else if (green && sc_Character.IdPlayer == idPlayer)
                {
                    ColorManager.Instance.playerGotGreen = true;
                    CameraManager.Instance.AnimationIsForGemme = true;
                    CameraManager.Instance.startAnimCameraToGemme(2);
                }
                fxGemmeRecup.Play();
                //Handheld.Vibrate();
                Destroy(gameObject);
            }
        }
    }

    private void RotationGemme()
    {
        transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
    }

    private IEnumerator AnimationGemme()
    {
        while (true)
        {
            float startTime = Time.time;
            Vector3 startPosition = transform.position;
            Vector3 endPosition = new Vector3(startPosition.x, startPosition.y + 0.7f, startPosition.z);
            while (Time.time < startTime + durationMove)
            {
                transform.position = Vector3.Lerp(startPosition, endPosition, AnimCurvMoveGemme.Evaluate((Time.time - startTime) / durationMove));
                yield return null;
            }
            transform.position = startPosition;
            yield return null;
        }
    }
}
