﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    private Animator animColor;
    public static ColorManager Instance { get; private set; }
    [HideInInspector] public bool playerGotBlue = false;
    [HideInInspector] public bool playerGotRed = false;
    [HideInInspector] public bool playerGotGreen = false;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        animColor = GetComponent<Animator>();
    }
    
    void Update()
    {
        if (Input.GetKey(KeyCode.B))
        {
            playerGotBlue = true;
            ChangeColorToBlue(true);
        }
        if (Input.GetKey(KeyCode.R))
        {
            playerGotRed = true;
            ChangeColorToRed(true);
        }
        if (Input.GetKey(KeyCode.G))
        {
            playerGotGreen = true;
            ChangeColorToGreen(true);
        }
    }

    public void ChangeColorToBlue(bool active)
    {
        if(playerGotBlue)
            animColor.SetBool("blue", active);
    }

    public void ChangeColorToRed(bool active)
    {
        if(playerGotRed)
            animColor.SetBool("red", active);
    }

    public void ChangeColorToGreen(bool active)
    {
        if(playerGotGreen)
            animColor.SetBool("green", active);
    }
}
