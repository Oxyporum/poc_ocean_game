﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorStele : MonoBehaviour
{
    [SerializeField] private GameObject SteleBrisee;
    [SerializeField] private GameObject Gemme;
    [SerializeField] private List<ParticleSystem> LiExplosionParticle;
    public Animator FxStellaColor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && CameraManager.Instance.IdTarget != 0)
        {
            Character sc_Character = other.GetComponent<Character>();
            if (sc_Character.canMove && sc_Character.SharkIsBoost)
            {
                ColorManager.Instance.ChangeColorToBlue(true);
                ColorManager.Instance.ChangeColorToRed(true);
                ColorManager.Instance.ChangeColorToGreen(true);
                SteleBrisee.SetActive(false);
               // Handheld.Vibrate();
                if (ColorManager.Instance.playerGotBlue || ColorManager.Instance.playerGotRed || ColorManager.Instance.playerGotGreen)
                {
                    Gemme.SetActive(true);

                    foreach(ParticleSystem explosion in LiExplosionParticle)
                        explosion.Play();

                    if (ColorManager.Instance.playerGotBlue)
                    {
                        StartCoroutine(WaitToCameraAnimation(3));
                        ColorManager.Instance.playerGotBlue = false;
                    }
                    if (ColorManager.Instance.playerGotRed)
                        ColorManager.Instance.playerGotRed = false;
                    if (ColorManager.Instance.playerGotGreen)
                        ColorManager.Instance.playerGotGreen = false;
                }
            }
        }
    }

    private IEnumerator WaitToCameraAnimation(int idPos)
    {
        yield return new WaitForSeconds(2f);
        CameraManager.Instance.startAnimCameraToGemme(idPos);
    }
}
