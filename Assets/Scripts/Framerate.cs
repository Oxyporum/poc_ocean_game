﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Framerate : MonoBehaviour
{
    [Range(0, 120)]
    public int _framesPerSecond = 60;
    [Range(0,5)]
    public int _vSyncCount = 1;
    public bool _changeOnPlay;

    [SerializeField]
    private Text _fpsText;
    private float _deltaTime;

    void Awake()
    {
        if (_framesPerSecond != 0)
            Application.targetFrameRate = _framesPerSecond;

        QualitySettings.vSyncCount = _vSyncCount;
    }

    void Update()
    {

        #if UNITY_EDITOR
        if(_changeOnPlay)
        {
            if (_framesPerSecond != 0)
                Application.targetFrameRate = _framesPerSecond;

            QualitySettings.vSyncCount = _vSyncCount;
        }
#endif

        if (_fpsText != null)
            GetFPS();
    }

    private void GetFPS()
    {
        _deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;
        float fps = 1.0f / _deltaTime;
        _fpsText.text = Mathf.Ceil(fps).ToString() + " FPS";
    }
}
