﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get; private set; }

    [SerializeField] private AudioSource AudioSourceBuble;
    [SerializeField] private List<AudioClip> LiAudioBuble;
    [SerializeField] private float DurationBetweenBuble;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        StartCoroutine(BoucleBubleSound());
    }
    
    void Update()
    {
        
    }

    private IEnumerator BoucleBubleSound()
    {
        int idAudioBuble = 0;
        while (true)
        {
            AudioSourceBuble.clip = LiAudioBuble[idAudioBuble];
            AudioSourceBuble.Play();
            while (AudioSourceBuble.isPlaying)
            {
                yield return null;
            }

            idAudioBuble++;

            if (idAudioBuble > LiAudioBuble.Count - 1)
                idAudioBuble = 0;
            yield return new WaitForSeconds(DurationBetweenBuble);
            yield return null;
        }
    }
}
